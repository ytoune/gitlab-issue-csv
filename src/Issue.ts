import { Immutable } from './Immutable'
import { Readonly } from './Readonly'
import { fixIndent } from './fixIndent'

export type IssueBody = {
	title: string
	description: string
	due: string
	label: string[]
	assign: string
	milestone: string
	weight: string
	relate: string[]
}

export type IssueJSON = Readonly<Pick<IssueBody, 'title'> & Partial<IssueBody>>

export class Issue extends Immutable<IssueBody> {
	static create(title: string) {
		return new Issue({ title })
	}
	isValid() {
		const t = this._get('title')?.trim()
		return !!(t && !t.includes('\n'))
	}
	title(title: string) {
		return this._set('title', title)
	}
	description(description: string) {
		return this._set('description', description)
	}
	due(due: string) {
		return this._set('due', due)
	}
	label(label: 'To Do' | 'Doing' | string) {
		const v = this._get('label') || []
		return v.includes(label) ? this : this._set('label', [...v, label])
	}
	assign(assign: 'me' | string) {
		return this._set('assign', assign)
	}
	milestone(milestone: string) {
		return this._set('milestone', milestone)
	}
	weight(weight: number) {
		return this._set('weight', weight < 0 ? '' : String(weight))
	}
	relate(relate: string) {
		const v = this._get('relate') || []
		return v.includes(relate) ? this : this._set('relate', [...v, relate])
	}
	getLine() {
		const parts: string[] = []
		const description = fixIndent(this._get('description') || '').trim()
		if (description) parts.push(description + '\n')
		const relate = this._get('relate')
		if (relate?.length) parts.push(`/relate ${relate.join(' ')}`)
		const due = this._get('due')
		if (due) parts.push(`/due "${due}"`)
		const weight = this._get('weight')
		if (weight) parts.push(`/weight ${weight}`)
		const label = this._get('label')?.map(t => `~"${t}"`)
		if (label?.length) parts.push(`/label ${label.join(' ')}`)
		const milestone = this._get('milestone')
		if (milestone) parts.push(`/milestone %"${milestone}"`)
		const assign = this._get('assign')
		if (assign) parts.push(`/assign "${assign}"`)
		const title = this._get('title') || ''
		return [title, parts.filter(Boolean).join('\n')]
			.map(t => t.trim())
			.map(t =>
				t
					.split(/\r?\n/giu)
					.join('  \r\n')
					.replace(/\n {2}\r/giu, '\n\r')
					.replace(/"/giu, '""'),
			)
			.map(t => `"${t}"`)
			.join(',')
	}
}
