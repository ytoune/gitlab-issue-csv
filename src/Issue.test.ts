import { Issue } from './Issue'

describe('Issue.getLine', () => {
	test('作れる', () => {
		expect(Issue.create('ok').getLine()).toBe('"ok",""')
	})
	test('説明文は改行を含んでもおｋ', () => {
		expect(
			Issue.create('hoge').description('# hoge\n\n- [ ] piyo').getLine(),
		).toBe('"hoge","# hoge  \r\n\r\n- [ ] piyo"')
	})
	test('説明文が全体的にインデントされてたら消す', () => {
		expect(
			Issue.create('indented')
				.description(
					`
						# hoge


						## fuga

							- ok
					`,
				)
				.getLine(),
		).toBe('"indented","# hoge  \r\n\r\n\r\n## fuga  \r\n\r\n  - ok"')
	})
})

describe('Issue.isValid', () => {
	test('タイトルあるならおｋ', () => {
		expect(Issue.create('ok').isValid()).toBe(true)
	})
	test('タイトル無いなら不正', () => {
		expect(Issue.create('').isValid()).toBe(false)
	})
	test('タイトルに改行含むなら不正', () => {
		expect(Issue.create('hoge\nhoge').isValid()).toBe(false)
	})
	test('タイトルは末尾改行含んでもおｋ', () => {
		expect(Issue.create('hoge\n').isValid()).toBe(true)
	})
})
