import { create } from './create'

export class Immutable<Body extends Record<string, unknown>> {
	constructor(body?: Partial<Body>)
	constructor(private _body: Partial<Body> = {}) {}
	protected _set<K extends keyof Body>(key: K, val: Body[K]) {
		const clone = create(this)
		clone._body = { ...this._body, [key]: val }
		return clone
	}
	protected _get<K extends keyof Body>(key: K): Body[K] | undefined {
		return this._body[key]
	}
	toJSON(): Readonly<Partial<Body>> {
		return { ...this._body }
	}
}
