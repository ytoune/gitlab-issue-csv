export const create = <T>(obj: T): T =>
	Object.create(Object.getPrototypeOf(obj))
