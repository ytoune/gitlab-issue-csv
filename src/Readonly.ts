export type Readonly<T> = {
	readonly [P in keyof T]: T[P] extends (infer U)[] ? readonly U[] : T[P]
}
