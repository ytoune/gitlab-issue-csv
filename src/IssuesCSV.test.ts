import { Issue } from './Issue'
import { IssuesCSV } from './IssuesCSV'

describe('IssuesCSV', () => {
	test('作れる', () => {
		expect(IssuesCSV.create([Issue.create('ok')]).build()).toBe(
			'title,description\r\n"ok",""',
		)
	})
	test('空ならヘッダを出さない', () => {
		expect(IssuesCSV.create([]).build()).toBe('')
	})
})
