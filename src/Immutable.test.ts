import { Immutable } from './Immutable'

class Mod<T extends Record<string, unknown>> extends Immutable<T> {
	set2<K extends keyof T>(key: K, val: T[K]) {
		return this._set<K>(key, val)
	}
}

describe('Immutable.toJSON', () => {
	test('{}', () => {
		const obj = new Mod<{ name: string }>()
		expect(obj.toJSON()).toEqual({})
	})
	test('{name}', () => {
		const obj = new Mod<{ name: string }>()
		expect(obj.set2('name', 'ok').toJSON()).toEqual({ name: 'ok' })
		expect(obj.toJSON()).toEqual({})
	})
	test('{name,list}', () => {
		const obj = new Mod<{ name: string; list: number[] }>({ name: 'ok' })
		expect(obj.set2('list', [1, 2, 3]).toJSON()).toEqual({
			name: 'ok',
			list: [1, 2, 3],
		})
		expect(obj.toJSON()).toEqual({ name: 'ok' })
	})
})
