import { Immutable } from './Immutable'
import { Readonly } from './Readonly'
import { Issue, IssueJSON } from './Issue'

export type IssuesJSON = Readonly<{
	issues: IssueJSON[]
}>

export type IssuesBody = {
	issues: Issue[]
}

export class IssuesCSV extends Immutable<IssuesBody> {
	static create(issues: Issue[]) {
		return new IssuesCSV({ issues: [...issues] })
	}
	add(issue: Issue) {
		return this.addMany([issue])
	}
	addMany(issues: Issue[]) {
		return this._set('issues', [...(this._get('issues') || []), ...issues])
	}
	build() {
		const v = this._get('issues')
		return !v?.length
			? ''
			: 'title,description\r\n' +
					v
						.filter(i => i.isValid())
						.map(i => i.getLine())
						.join('\r\n')
	}
}
