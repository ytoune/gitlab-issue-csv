export const fixIndent = (text: string) => {
	const parts = text.replace(/\t/giu, '  ').split(/\n/giu)
	const min = parts.reduce((min, p) => {
		if (p.trim()) {
			const u = p.length - p.trimLeft().length
			if (u < min) return u
		}
		return min
	}, Infinity)
	return parts.map(p => p.slice(min)).join('\n')
}
