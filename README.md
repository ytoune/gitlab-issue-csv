# gitlab-issue-csv

gitlab の issues ページの import csv 機能用

```sh
yarn add gitlab:ytoune/gitlab-issue-csv#$VERSION
```

```ts
import * as fs from 'fs'
import { Issue, IssuesCSV } from 'gitlab-issue-csv'

const csvtext: string = IssuesCSV.create([
  Issue.create('make headers readable')
    .description(`
      - [ ] change text size
    `)
    .due('in 1 month')
    .label('To Do')
    .assign('me')
    .milestone('correction-of-appearance-20200831')
    .weight(180)
    .relate('#290'),
  Issue.create('make the logo cool')
    .description(`
      - [ ] change line color
    `)
    .due('in 1 month')
    .label('To Do')
    .assign('@ytoune')
    .milestone('correction-of-appearance-20200831')
    .weight(60)
    .relate('#290'),
]).build()

fs.writeFileSync('issues.csv', csvtext)
```
